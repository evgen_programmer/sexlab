﻿using System.Xml.Serialization;

namespace Grand.Services.ExportImport.Contracts
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class package
    {

        private string packNameField;

        private string timestampField;

        private packageCatalog_Item[] catalogField;

        /// <remarks/>
        public string PackName {
            get {
                return this.packNameField;
            }
            set {
                this.packNameField = value;
            }
        }

        /// <remarks/>
        public string timestamp {
            get {
                return this.timestampField;
            }
            set {
                this.timestampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Catalog_Item", IsNullable = false)]
        public packageCatalog_Item[] Catalog {
            get {
                return this.catalogField;
            }
            set {
                this.catalogField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class packageCatalog_Item
    {

        private ushort catIDField;

        private ushort parentIDField;

        private string titleField;

        /// <remarks/>
        public ushort catID {
            get {
                return this.catIDField;
            }
            set {
                this.catIDField = value;
            }
        }

        /// <remarks/>
        public ushort parentID {
            get {
                return this.parentIDField;
            }
            set {
                this.parentIDField = value;
            }
        }

        /// <remarks/>
        public string Title {
            get {
                return this.titleField;
            }
            set {
                this.titleField = value;
            }
        }
    }



    //[System.Xml.Serialization.XmlRootAttribute("Catalog_Item", Namespace = "", IsNullable = false)]
    //public class Catalog_Item
    //{
    //    [System.Xml.Serialization.XmlElementAttribute("catID")]
    //    public int Id { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("parentID")]
    //    public int ParentId { get; set; }

    //    [System.Xml.Serialization.XmlElementAttribute("Title")]
    //    public string Title { get; set; }
    //}

    //[System.Xml.Serialization.XmlRootAttribute("Catalog", Namespace = "", IsNullable = false)]
    //public class Catalog
    //{
    //    [XmlArrayItem(typeof(Catalog_Item))] 
    //    public Catalog_Item[] CatalogItems { get; set; }
    //}

    //[System.Xml.Serialization.XmlRootAttribute("package", Namespace = "", IsNullable = false)]
    //public class Package
    //{
    //    [System.Xml.Serialization.XmlElementAttribute("Catalog")]
    //    public Catalog Catalog { get; set; }
    //}
}
