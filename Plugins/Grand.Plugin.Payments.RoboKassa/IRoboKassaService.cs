using System.Threading.Tasks;
using Grand.Core.Domain.Orders;

namespace Grand.Plugin.Payments.RoboKassa
{
    public interface IRoboKassaService
    {
        string GetPaymentUrl(Order order);
        Task<string> SuccessCallback(string out_summ, int inv_id, string signatureValue);
    }
}