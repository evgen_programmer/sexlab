﻿using System.Threading.Tasks;
using Grand.Core;
using Grand.Plugin.Payments.RoboKassa.Models;
using Grand.Services.Configuration;
using Grand.Services.Localization;
using Microsoft.AspNetCore.Mvc;

namespace Grand.Plugin.Payments.RoboKassa.Components
{
    [ViewComponent(Name = "PaymentRoboKassaComponent")]
    public class PaymentRoboKassaViewComponent : ViewComponent
    {
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public PaymentRoboKassaViewComponent(IWorkContext workContext,   
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _workContext = workContext;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var robokassaPaymentSettings = _settingService.LoadSetting<RoboKassaSettings>(_storeContext.CurrentStore.Id);

            var model = new PaymentInfoModel
            {
                DescriptionText = robokassaPaymentSettings.GetLocalizedSetting(_settingService, x => 
                    x.DescriptionText, _workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id)
            };
            return View("~/Plugins/Payments.RoboKassa/Views/PaymentRoboKassa/PaymentInfo.cshtml", await Task.FromResult(model));
        }
    }
}