﻿using Grand.Core.Domain.Orders;
using System;
using System.Globalization;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Grand.Plugin.Payments.RoboKassa.Models;
using Newtonsoft.Json;
using ThirdParty.Json.LitJson;
using ArgumentException = System.ArgumentException;
using Grand.Services.Orders;

namespace Grand.Plugin.Payments.RoboKassa
{
    public class RoboKassaService: IRoboKassaService
    {
        private readonly RoboKassaSettings _settings;
        private readonly IOrderService _orderSerice;
        private readonly IOrderProcessingService _orderProcessingService;

        public RoboKassaService(RoboKassaSettings settings, IOrderService orderService, IOrderProcessingService orderProcessingService)
        {
            _settings = settings;
            _orderSerice = orderService;
            _orderProcessingService = orderProcessingService;
        }

        public string GetPaymentUrl(Order order)
        {
            var merchantId = _settings.MerchantLogin;
            var sum = order.OrderTotal.ToString(CultureInfo.CurrentCulture);
            var invId = order.OrderNumber.ToString();
            var signatureValue = MakeHash(_settings.MerchantLogin, sum, invId);

            var url = $"https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin={merchantId}&InvId={invId}" +
                      $"&Culture=ru&Encoding=utf-8&OutSum={sum}&SignatureValue={signatureValue}";

            return url;
        }

        public async Task<string> SendSecondCheck()
        {
            var secondCheckModel = new SecondCheckModel();
            var json = JsonConvert.SerializeObject(secondCheckModel);
            var encodedString = json.Base64() + _settings.MerchantPassword;
            var hashed = encodedString.SHA512();
            var resultEncoded = hashed.Base64();
            var uri = "";
            
            var httpClient = new HttpClient();
            var response = await httpClient.PostAsync(uri, new StringContent(resultEncoded));

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
            
            return "Error";
        }
        
        public async Task<string> SuccessCallback(string out_summ, int inv_id, string signatureValue)
        {
            var order = await _orderSerice.GetOrderByNumber(inv_id);

            if (order == null) 
                return "";

            var sum = order.OrderTotal.ToString("#.##0000", CultureInfo.CreateSpecificCulture("en-GB"));
            var invId = order.OrderNumber.ToString();
            var calcHash = MakeHash2(sum, invId, true).ToLower();
            var inputHashNormalized = signatureValue.ToLower();
            
            if (!string.Equals(calcHash, inputHashNormalized))
            {
                throw new ArgumentException("Hash is invalid");
            }

            if(_orderProcessingService.CanMarkOrderAsPaid(order).Result)
            {
                await _orderProcessingService.MarkOrderAsPaid(order);
                await _orderProcessingService.CheckOrderStatus(order);
            }


            return $"OK{inv_id}";
        }
        
        private string MakeHash(string merchantId, string outSum, string invId, bool usePass2 = false)
        {
            var pass = usePass2 ? _settings.CheckPassword : _settings.MerchantPassword;
            
            var value = $"{merchantId}:{outSum}:{invId}:{pass}";
            
            return value.Trim(':').SHA512();
        }

        private string MakeHash2(string outSum, string invId, bool usePass2 = false)
        {
            var pass = usePass2 ? _settings.CheckPassword : _settings.MerchantPassword;

            var value = $"{outSum}:{invId}:{pass}";

            return value.Trim(':').SHA512();
        }
    }
}
