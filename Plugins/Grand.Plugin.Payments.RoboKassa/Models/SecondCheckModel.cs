namespace Grand.Plugin.Payments.RoboKassa.Models
{
    // todo: need to rename if api is not case sensitive
    public class SecondCheckModel
    {
        public string merchantId { get; set; }
        public int id { get; set; }
        public int originId { get; set; }
        public string operation { get; set; } = "sell";
        public string sno { get; set; } = "osn";
        public string url { get; set; } = "sexland.ru";
        public decimal total { get; set; }
        public item[] items { get; set; } 
        public client client { get; set; }
        public payment[] payments { get; set; }
        public vat[] vats { get; set; }
    }

    public class item
    {
        public string name { get; set; }
        public int quantity { get; set; }
        public decimal sum { get; set; }
        public string tax { get; set; } = "none";
        public string payment_method { get; set; } = "full_payment";
        public string payment_object { get; set; } = "commodity";
    }

    public class client
    {
        public string email { get; set; }
        public string phone { get; set; }
    }

    public class payment
    {
        public string type { get; set; } = "2";
        public decimal sum { get; set; }
    }

    public class vat
    {
        public string type { get; set; } = "none";
        public decimal sum { get; set; }
    }
}