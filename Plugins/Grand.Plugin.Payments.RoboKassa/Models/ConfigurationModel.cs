﻿using System.Collections.Generic;
using Grand.Framework.Localization;
using Grand.Framework.Mvc.ModelBinding;
using Grand.Framework.Mvc.Models;

namespace Grand.Plugin.Payments.RoboKassa.Models
{
    public class ConfigurationModel : BaseGrandModel, ILocalizedModel<ConfigurationModel.ConfigurationLocalizedModel>
    {
        public ConfigurationModel()
        {
            Locales = new List<ConfigurationLocalizedModel>();
        }
        
        public string ActiveStoreScopeConfiguration { get; set; }

        [GrandResourceDisplayName("Description")]
        public string DescriptionText { get; set; }
        public bool DescriptionText_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.UseSandbox")]
        public bool UseSandbox { get; set; }
        public bool UseSandbox_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.BusinessEmail")]
        public string BusinessEmail { get; set; }
        public bool BusinessEmail_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.PDTValidateOrderTotal")]
        public bool PdtValidateOrderTotal { get; set; }
        public bool PdtValidateOrderTotal_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.AdditionalFee")]
        public decimal AdditionalFee { get; set; }
        public bool AdditionalFee_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.AdditionalFeePercentage")]
        public bool AdditionalFeePercentage { get; set; }
        public bool AdditionalFeePercentage_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Plugins.Payments.RoboKassa.Fields.PassProductNamesAndTotals")]
        public bool PassProductNamesAndTotals { get; set; }
        public bool PassProductNamesAndTotals_OverrideForStore { get; set; }



        [GrandResourceDisplayName("Merchant Login")]
        public string MerchantLogin { get; set; }
        public bool MerchantLogin_OverrideForStore { get; set; }
       
        [GrandResourceDisplayName("Merchant Password")]
        public string MerchantPassword { get; set; }
        public bool MerchantPassword_OverrideForStore { get; set; }

        [GrandResourceDisplayName("Check Password")]
        public string CheckPassword { get; set; }
        public bool CheckPassword_OverrideForStore { get; set; }
        
        [GrandResourceDisplayName("Is Test")]
        public string IsTest { get; set; }
        public bool IsTest_OverrideForStore { get; set; }
        
        public IList<ConfigurationLocalizedModel> Locales { get; set; }
        
        #region Nested class

        public partial class ConfigurationLocalizedModel : ILocalizedModelLocal
        {
            public string LanguageId { get; set; }
            
            [GrandResourceDisplayName("Plugins.Payment.RoboKassa.DescriptionText")]
            public string DescriptionText { get; set; }
        }

        #endregion
    }
}