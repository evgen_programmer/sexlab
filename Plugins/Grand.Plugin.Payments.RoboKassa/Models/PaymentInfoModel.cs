﻿using Grand.Framework.Mvc.Models;

namespace Grand.Plugin.Payments.RoboKassa.Models
{
    public class PaymentInfoModel : BaseGrandModel
    {
        public string DescriptionText { get; set; }
    }
}