﻿using Grand.Framework.Mvc.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Grand.Plugin.Payments.RoboKassa
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            //PDT
            routeBuilder.MapRoute("Plugin.Payments.RoboKassa.PDTHandler",
                 "Plugins/PaymentRoboKassa/PDTHandler",
                 new { controller = "PaymentRoboKassa", action = "PDTHandler" }
            );
            //IPN
            routeBuilder.MapRoute("Plugin.Payments.RoboKassa.IPNHandler",
                 "Plugins/PaymentRoboKassa/IPNHandler",
                 new { controller = "PaymentRoboKassa", action = "IPNHandler" }
            );
            //Cancel
            routeBuilder.MapRoute("Plugin.Payments.RoboKassa.CancelOrder",
                 "Plugins/PaymentRoboKassa/CancelOrder",
                 new { controller = "PaymentRoboKassa", action = "CancelOrder" }
            );

            routeBuilder.MapRoute("Plugin.Payments.RoboKassa.SuccessCallback",
                "Plugins/PaymentRoboKassa/SuccessCallback",
                new {controller = "PaymentRoboKassa", action = "SuccessCallback"});

            routeBuilder.MapRoute("Plugin.Payments.RoboKassa.ReturnCallback",
                "Plugins/PaymentRoboKassa/ReturnCallback",
                new { controller = "PaymentRoboKassa", action = "ReturnCallback" });

        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
