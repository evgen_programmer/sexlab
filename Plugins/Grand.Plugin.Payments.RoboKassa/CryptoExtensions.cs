using System;
using System.Text;

namespace Grand.Plugin.Payments.RoboKassa
{
    public static class CryptoExtensions
    {
        public static string SHA512(this string input)
        {
            var bytes = System.Text.Encoding.ASCII.GetBytes(input);
            using (var hash = System.Security.Cryptography.SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);
                var hashedInputStringBuilder = new System.Text.StringBuilder(128);
                foreach (var b in hashedInputBytes)
                {
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                }
                return hashedInputStringBuilder.ToString().ToLower();
            }
        }

        public static string Base64(this string input)
        {
            var encodedArray = Encoding.UTF8.GetBytes(input);
            var result = Convert.ToBase64String(encodedArray);
            
            var builder = new StringBuilder(result);
            
            while (builder[result.Length - 1] == '=')
            {
                builder.Length--;
            }
            
            return builder.ToString();
        }
    }
}