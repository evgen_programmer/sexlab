using Grand.Core.Configuration;

namespace Grand.Plugin.Payments.RoboKassa
{
    public class RoboKassaSettings : ISettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether shippable products are required in order to display this payment method during checkout
        /// </summary>
        public bool ShippableProductRequired { get; set; }
        public string DescriptionText { get; set; }
        public bool UseSandbox { get; set; }
        public string BusinessEmail { get; set; }
        // public string PdtToken { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether to "additional fee" is specified as percentage.
        /// true - percentage, false - fixed value.
        /// </summary>
        public bool AdditionalFeePercentage { get; set; }
        /// <summary>
        /// Additional fee
        /// </summary>
        public decimal AdditionalFee { get; set; }
        public bool PassProductNamesAndTotals { get; set; }
        public bool PdtValidateOrderTotal { get; set; }
        
        //add robokassa settings
        //public string RoboKassMode { get; set; }
       
        public string MerchantLogin { get; set; }
        public string MerchantPassword { get; set; }
        public string CheckPassword { get; set; }
        public string HashMethod { get; set; }
        public string IsTest { get; set; }
    }
}
