﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Grand.Plugin.Misc.Integration.Helper
{
    public static class StringClearHelper
    {
        public static string Clear(this String str)
        { 
            return Regex.Replace(str, "[^0-9A-Za-zА-Яа-я ,]", "");
        }

        public static string ClearFromJson(this String str)
        {
            if (String.IsNullOrEmpty(str) || str.Length <= 2
                || str.Substring(0, 1) != "\"")
                return str;

            return str.Substring(1, str.Length - 1).Substring(0, str.Length - 2); 
        }
    }
}
