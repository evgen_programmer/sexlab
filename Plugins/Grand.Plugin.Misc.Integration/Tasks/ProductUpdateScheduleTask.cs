﻿using Grand.Core;
using Grand.Core.Domain.Catalog;
using Grand.Core.Domain.Logging;
using Grand.Core.Domain.Media;
using Grand.Core.Domain.Tasks;
using Grand.Services.Catalog;
using Grand.Services.Logging;
using Grand.Services.Media;
using Grand.Services.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Grand.Services.Vendors;
using Grand.Plugin.Misc.Integration.Helper;
using Grand.Services.Seo;
using Grand.Services.Localization;
using Grand.Core.Domain.Seo;
using Grand.Services.Directory;
using Grand.Services.Tax;

namespace Grand.Plugin.Misc.Integration
{
    public partial class ProductUpdateScheduleTask : ScheduleTask, IScheduleTask
    {
        private readonly ILogger _logger;
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IPictureService _pictureService;
        private readonly IVendorService _vendorService;
        private readonly IUrlRecordService _urlRecordService;  
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ILanguageService _languageService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreContext _storeContext;
        private readonly ICategoryTemplateService _categoryTemplateService;
        private readonly IManufacturerTemplateService _manufacturerTemplateService;
        private readonly IDownloadService _downloadService;
        private readonly ITaxCategoryService _taxService;
        private readonly IMeasureService _measureService;
        private readonly SeoSettings _seoSetting;

        private readonly object _lock = new object();

        public ProductUpdateScheduleTask(IProductService productService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            ISpecificationAttributeService specificationAttributeService,
            IPictureService pictureService,
            IUrlRecordService urlRecordService,
            IStoreContext storeContext,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IVendorService vendorService,
            ICategoryTemplateService categoryTemplateService,
            IManufacturerTemplateService manufacturerTemplateService,
            IProductTemplateService productTemplateService,
            IDownloadService downloadService,
            ITaxCategoryService taxService,
            IMeasureService measureService,
            IProductAttributeService productAttributeService,
            ILanguageService languageService,
            ILogger logger,
            SeoSettings seoSetting)
        {
            _specificationAttributeService = specificationAttributeService;
            _productService = productService;
            _categoryService = categoryService;
            _manufacturerService = manufacturerService;
            _pictureService = pictureService;
            _urlRecordService = urlRecordService;
            _storeContext = storeContext;
            _vendorService = vendorService;
            _categoryTemplateService = categoryTemplateService;
            _manufacturerTemplateService = manufacturerTemplateService;
            _productTemplateService = productTemplateService;
            _downloadService = downloadService;
            _taxService = taxService;
            _measureService = measureService;
            _languageService = languageService;
            _productAttributeService = productAttributeService;
            _logger = logger;
            _seoSetting = seoSetting;
        }

        private IList<SpecificationAttribute> CreateAttributes(string[] attributes)
        {
            var cache = _specificationAttributeService.GetSpecificationAttributes().Result;
            foreach (var a in attributes)
            {
                var s = cache.FirstOrDefault(c => c.Name == a);
                if (s == null)
                {
                    _specificationAttributeService.InsertSpecificationAttribute(new SpecificationAttribute() {
                        Name = a,
                        DisplayOrder = 0
                    }).Wait();
                }
            }

            return _specificationAttributeService.GetSpecificationAttributes().Result;
        }

        private IList<ProductAttribute> CreateProductAttributes(string[] productAttributes)
        {
            var cache = _productAttributeService.GetAllProductAttributes().Result;
            foreach (var a in productAttributes)
            {
                var s = cache.FirstOrDefault(c => c.Name == a);
                if (s == null)
                {
                    _productAttributeService.InsertProductAttribute(new ProductAttribute() {
                        Name = a
                    });
                }
            }

            return _productAttributeService.GetAllProductAttributes().Result;;
        }

        private Product AddAttributeToProduct(string arrtibuteName, string attributeOption, IList<SpecificationAttribute> cache, Product product)
        {
            var atribute = cache.FirstOrDefault(p => p.Name == arrtibuteName);
            if (atribute == null)
            {
                return product;
            }

            var option = atribute.SpecificationAttributeOptions.FirstOrDefault(p => p.Name == attributeOption);

            if (option == null)
            {
                atribute.SpecificationAttributeOptions.Add(new SpecificationAttributeOption() {
                    Name = attributeOption
                });;

                _specificationAttributeService.UpdateSpecificationAttribute(atribute).Wait();
                option = atribute.SpecificationAttributeOptions.FirstOrDefault(p => p.Name == attributeOption);
            }

            var exists = product.ProductSpecificationAttributes.FirstOrDefault(c => c.SpecificationAttributeId == atribute.Id);

            if (exists != null)
            {
                product.ProductSpecificationAttributes.Remove(exists);
            }

            product.ProductSpecificationAttributes.Add(new ProductSpecificationAttribute() {
                SpecificationAttributeOptionId = option.Id,
                SpecificationAttributeId = atribute.Id,
                DisplayOrder = 0,
                AllowFiltering = true,
                ShowOnProductPage = true
            });

            return product;
        }

        private Product AddProductAttributeToProduct(string arrtibuteName, string[] attributeOptions, IList<ProductAttribute> cache, Product product)
        {
            var atribute = cache.FirstOrDefault(p => p.Name == arrtibuteName);
            if (atribute == null)
            {
                return product;
            }

            var productAttributeMapping = new ProductAttributeMapping() 
            {
                AttributeControlType = AttributeControlType.DropdownList,
                ProductAttributeId = atribute.Id,
                DisplayOrder = 0,
                IsRequired = true
            };

            int counter = 0;
          
            foreach (var at in attributeOptions)
            {
                productAttributeMapping.ProductAttributeValues.Add(new ProductAttributeValue() {
                    Name = at,
                    IsPreSelected = counter == 0
                });
                counter++;
            }

            product.ProductAttributeMappings.Add(productAttributeMapping);
            return product;
        }

        public async Task Execute()
        {
            lock (_lock)
            {
                try
                {
                    string[] attributes = { "Цвет", "Батарейки", "Материал", "Упаковка", "Коллекция", "Длина", "Вес", "Диаметр", "Дата отгрузки"};
                    string[] productAttributes = { "Размер" };

                    var attributeCache = CreateAttributes(attributes);
                    var productAttributesCache = CreateProductAttributes(productAttributes);

                    var categogiesCache = _categoryService.GetAllCategories().Result;
                    int count = 0;

                    using (var httpClient = new HttpClient())
                    {
                        var response = httpClient.GetAsync("http://stripmag.ru/datafeed/insales_full_cp1251.csv").Result;
      
                        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                        var encoding = Encoding.GetEncoding(1251);

                        using (var reader = new StreamReader(response.Content.ReadAsStreamAsync().Result, encoding))
                        {
                            while (!reader.EndOfStream)
                            {
                                string line = reader.ReadLine();

                                if (String.IsNullOrWhiteSpace(line) || count == 0)
                                {
                                    count++;
                                    continue;
                                }

                                string[] tmp = line.Split(';');

                                //parse
                                var prodID = tmp[0].Trim();
                                var aID = tmp[1].Trim();
                                var Barcode = tmp[2].Trim().ClearFromJson();
                                var Manufacture = tmp[3].Trim().ClearFromJson();
                                var Name = tmp[5].Trim().ClearFromJson();
                                var RetailPrice = Decimal.Parse(tmp[6].Trim());
                                var BaseRetailPrice = Decimal.Parse(tmp[7].Trim());
                                var WholePrice = Decimal.Parse(tmp[8].Trim());
                                var BaseWholePrice = Decimal.Parse(tmp[9].Trim());
                                var Discount = Decimal.Parse(tmp[10].Trim());
                                var InSale = int.Parse(tmp[11].Trim());
                                var ShippingDate = tmp[11].Trim();
                                var Description = tmp[13].Trim().ClearFromJson();
                                var Brutto = String.IsNullOrEmpty(tmp[14].Trim()) ? decimal.Zero : Decimal.Parse(tmp[14].Trim());
                                var Batteries = tmp[15].Trim().ClearFromJson();
                                var Pack = tmp[16].Trim();
                                var Material = tmp[17].Trim().ClearFromJson();
                                var Lenght = tmp[18].Trim().ClearFromJson();
                                var Diameter = tmp[19].Trim().ClearFromJson();
                                var Collection = tmp[20].Trim().ClearFromJson();
                                var Image = tmp[21].Trim().ClearFromJson();
                                var Category = tmp[22].Trim().ClearFromJson();
                                var SubCategory = tmp[23].Trim().ClearFromJson();
                                var Color = tmp[24].Trim().ClearFromJson();
                                var Size = tmp[25].Trim().ClearFromJson();

                                var product = string.IsNullOrEmpty(aID) ? null : _productService.GetProductBySku(aID).Result;
                                var isNew = product == null;

                                if (isNew)
                                {
                                    //load pictures
                                    var pictures = LoadPicutres(Image.Split(" ")).Result;
                                    var categories = LoadProductCategories(Category, SubCategory, categogiesCache).Result;

                                    var templates = _productTemplateService.GetAllProductTemplates().Result;
                                    var taxes = _taxService.GetAllTaxCategories().Result;
                                    var units = _measureService.GetAllMeasureUnits().Result;

                                    product = new Product();
                                    product.Sku = aID;
                                    product.Acticle = prodID;
                                    product.Name = Name;
                                    product.FullDescription = Description;
                                    product.Weight = Brutto;
                                    product.Gtin = Barcode;
                                    product.CreatedOnUtc = DateTime.UtcNow;
                                    product.ProductTemplateId = templates.FirstOrDefault()?.Id;
                                    product.TaxCategoryId = taxes.FirstOrDefault()?.Id;
                                    product.UnitId = units.FirstOrDefault()?.Id;

                                    if (!String.IsNullOrEmpty(Manufacture))
                                    {
                                        var manufacture = _manufacturerService.GetAllManufacturers(manufacturerName: Manufacture).Result.FirstOrDefault();
                                        var isNewManufacture = manufacture == null;
                                        manufacture = manufacture ?? new Manufacturer();

                                        if (isNewManufacture)
                                        {
                                            var manufactureSename = string.Empty;
                                            var manufacturetemplates = _manufacturerTemplateService.GetAllManufacturerTemplates().Result;

                                            manufacture.Name = Manufacture;
                                            manufacture.CreatedOnUtc = DateTime.UtcNow;
                                            manufacture.UpdatedOnUtc = DateTime.UtcNow;

                                            manufacture.ManufacturerTemplateId = templates.FirstOrDefault()?.Id;
                                            manufactureSename = manufacture.ValidateSeName(manufactureSename, Transliteration.Front(manufacture.Name), true, _seoSetting, _urlRecordService, _languageService).Result;
                                            manufacture.SeName = manufactureSename;
                                            manufacture.Published = true;

                                            _manufacturerService.InsertManufacturer(manufacture).Wait();
                                            _urlRecordService.SaveSlug(manufacture, manufactureSename, "").Wait();
                                        }

                                        product.ProductManufacturers.Add(new ProductManufacturer() {
                                            DisplayOrder = 0,
                                            ManufacturerId = manufacture.Id
                                        });
                                    }

               
                                    foreach (var c in categories)
                                    {
                                        product.ProductCategories.Add(c);
                                    }

                                    foreach (var p in pictures)
                                    {
                                        product.ProductPictures.Add(p);
                                    }

                                    if (!String.IsNullOrEmpty(Lenght))
                                    {
                                        product = AddAttributeToProduct("Длина", $"{Lenght} см.", attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Brutto.ToString()) && Brutto != decimal.Zero)
                                    {
                                        product = AddAttributeToProduct("Вес", $"{Brutto.ToString()} кг.", attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Diameter))
                                    {
                                        product = AddAttributeToProduct("Диаметр", $"{Diameter} см.", attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Color))
                                    {
                                        product = AddAttributeToProduct("Цвет", Color, attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Batteries))
                                    {
                                        product = AddAttributeToProduct("Батарейки", Batteries, attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Material))
                                    {
                                        product = AddAttributeToProduct("Материал", Material, attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Size))
                                    {
                                        product = AddProductAttributeToProduct("Размер", Size.Split("-"), productAttributesCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Collection))
                                    {
                                        product = AddAttributeToProduct("Коллекция", Collection, attributeCache, product);
                                    }

                                    if (!String.IsNullOrEmpty(Pack))
                                    {
                                        product = AddAttributeToProduct("Упаковка", Pack, attributeCache, product);
                                    }

                                    var sename = string.Empty;
                                    sename = product.ValidateSeName(sename, Transliteration.Front(product.Name), true, _seoSetting, _urlRecordService, _languageService).Result;
                                    product.SeName = sename;
                                    product.AllowBackInStockSubscriptions = true;
                                
                                    product.VisibleIndividually = true;
                                    product.Published = true;
                                    product.ManageInventoryMethod = ManageInventoryMethod.ManageStock;
                                    product.ProductType = ProductType.SimpleProduct;
                                    product.LowStockActivity = LowStockActivity.DisableBuyButton;
                                    product.DisplayStockAvailability = true;
                                    product.DisplayStockQuantity = false;
                                    product.MinStockQuantity = 0;
                                    product.NotifyAdminForQuantityBelow = 1;
                                    product.OrderMinimumQuantity = 1;
                                    product.IsShipEnabled = true;
                                    product.AllowCustomerReviews = true;

                                    _productService.InsertProduct(product).Wait();
                                    _urlRecordService.SaveSlug(product, sename, "").Wait();
                                }

                                if (!String.IsNullOrEmpty(ShippingDate))
                                {
                                    product = AddAttributeToProduct("Дата отгрузки", ShippingDate.Split(' ')[0], attributeCache, product);
                                }

                                product.StockQuantity = InSale;
                                product.LimitedToStores = false;
                                product.Price = RetailPrice;
                                product.OldPrice = BaseRetailPrice;
                                product.ProductCost = BaseWholePrice;
                                product.OrderMaximumQuantity = InSale;
                                product.LowStock = InSale == 0;
                                product.DisableBuyButton = InSale == 0;
                                product.DisableWishlistButton = true;
                                product.UpdatedOnUtc = DateTime.UtcNow;
                                product.Published = product.Published ? product.Published : InSale == 0;

                                _productService.UpdateProduct(product).Wait();

                                count++;
                            }
                        }
                    }

                    Log(LogLevel.Information, "Обновление продуктов успешно завершено", $"Обновлено {count} продуктов").Wait();
                }
                catch (Exception ex)
                {
                    Log(LogLevel.Error, "Ошибка при обновлении продуктов", ex.Message).Wait();
                }
            }
        }

        private async Task<List<ProductCategory>> LoadProductCategories(string mainCategoryStr, string subCategoryStr, IPagedList<Category> cache)
        {
            var result = new List<ProductCategory>();
            if (!String.IsNullOrEmpty(mainCategoryStr))
            {
                var mainCategory = cache.FirstOrDefault(c => c.Name.ToLower() == mainCategoryStr.ToLower());
                if (mainCategory != null)
                {
                    result.Add(new ProductCategory() {
                        CategoryId = mainCategory.Id,
                        DisplayOrder = 0
                    });
                }
            }

            if (!String.IsNullOrEmpty(subCategoryStr))
            {
                var subCategory = cache.FirstOrDefault(c => c.Name.ToLower() == subCategoryStr.ToLower());
                if (subCategory != null)
                {
                    result.Add(new ProductCategory() {
                        CategoryId = subCategory.Id,
                        DisplayOrder = 0
                    });
                }
            }

            return result;
        }

        private async Task<List<ProductPicture>> LoadPicutres(string[] links)
        {
            var result = new List<ProductPicture>();
            using (var httpClient = new HttpClient()) 
            {
                foreach (var l in links)
                {
                    var response = await httpClient.GetAsync(l);
                    var picture = await _pictureService.InsertPicture(response.Content.ReadAsByteArrayAsync().Result, "image/jpeg" , "");
                    result.Add(new ProductPicture() { 
                        PictureId = picture.Id,
                        DisplayOrder = 0
                    });
                }
            }              

            return result;
        }

        private async Task Log(LogLevel logLevel, string message = "", string fullmessage = "")
        {
            await _logger.InsertLog(logLevel, message, fullmessage);
        }
    }
}
