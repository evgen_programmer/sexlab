using Grand.Core;
using Grand.Core.Data;
using Grand.Core.Domain.Tasks;
using Grand.Core.Plugins;
using Grand.Services.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Grand.Plugin.Misc.Integration
{
    /// <summary>
    /// Live person provider
    /// </summary>
    public class IntegrationPlugin : BasePlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly IServiceProvider _serviceProvider;
        private readonly IRepository<ScheduleTask> _scheduleTaskRepository;
        public IntegrationPlugin(IWebHelper webHelper, IServiceProvider serviceProvider, IRepository<ScheduleTask> scheduleTaskRepository)
        {
            this._webHelper = webHelper;
            this._serviceProvider = serviceProvider;
            this._scheduleTaskRepository = scheduleTaskRepository;
        }
        private async Task InstallTasks()
        {
            var tasks = new List<ScheduleTask>
            {           
                new ScheduleTask
                {
                    ScheduleTaskName = "���������� - �������� ��������",
                    Type = "Grand.Plugin.Misc.Integration.ProductUpdateScheduleTask, Grand.Plugin.Misc.Integration",
                    Enabled = true,
                    StopOnError = false,
                    TimeInterval = 32000
                }
            };
            await _scheduleTaskRepository.InsertAsync(tasks);
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override async Task Install()
        {         
            await InstallTasks();

            await base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override async Task Uninstall()
        {
            await base.Uninstall();
        }
    }
}
