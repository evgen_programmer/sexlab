﻿using Autofac;
using Grand.Core.Configuration;
using Grand.Core.Infrastructure;
using Grand.Core.Infrastructure.DependencyManagement;
using Grand.Services.Tasks;

namespace Grand.Plugin.Misc.Integration
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public virtual void Register(ContainerBuilder builder, ITypeFinder typeFinder, GrandConfig config)
        {
            builder.RegisterType<IntegrationPlugin>().InstancePerLifetimeScope();
            builder.RegisterType<ProductUpdateScheduleTask>().As<IScheduleTask>().InstancePerLifetimeScope();
        }

        public int Order
        {
            get { return 10; }
        }
    }

}
