﻿using Grand.Core.Domain.Orders;
using Grand.Core.Events;
using Grand.Services.Catalog;
using MediatR;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Grand.Services.Logging;
using Grand.Core.Domain.Logging;
using System.Globalization;

namespace Grand.Plugin.Misc.Integration.EventHandler
{
    public class OrderEventHandler : INotificationHandler<EntityInserted<Order>>, 
        INotificationHandler<OrderPaidEvent>
    {
        private readonly IProductService _productService;
        private readonly ILogger _logger; 

        public OrderEventHandler(IProductService productService,
            ILogger logger)
        {
            _productService = productService;
            _logger = logger;
        }

        public async Task Handle(EntityInserted<Order> notification, CancellationToken cancellationToken)
        {
            if (notification.Entity.PaymentMethodSystemName == "Payments.RoboKassa")
            {
                return;
            }

            Task.Run(() => SendOrder(notification.Entity));
        }

        public async Task Handle(OrderPaidEvent notification, CancellationToken cancellationToken)
        {
            if (notification.Order.PaymentMethodSystemName != "Payments.RoboKassa"
               || (notification.Order.PaymentMethodSystemName == "Payments.RoboKassa" &&
               notification.Order.PaymentStatus != Core.Domain.Payments.PaymentStatus.Paid))
            {
                return;
            }

            Task.Run(() => SendOrder(notification.Order));
        }

        public async Task SendOrder(Order order)
        {           
            try
            {
                
                var queryParameters = new Dictionary<string, string>();
                if(order.ShippingMethod == "Пикап в Самовывоз")
                {
                    queryParameters.Add("dsFio", $"{order.BillingAddress.LastName} {order.BillingAddress.FirstName}");
                    queryParameters.Add("dsEmail", order.BillingAddress.Email);
                    queryParameters.Add("dsMobPhone", order.BillingAddress.PhoneNumber);
                    queryParameters.Add("dsCity", "Москва");
                    queryParameters.Add("dsStreet", "ул. Автозаводская");
                    queryParameters.Add("dsHouse", "дом 16");
                    queryParameters.Add("dsFlat", "корпус 2, строение 8");
                }
                else
                {
                    queryParameters.Add("dsFio", $"{order.ShippingAddress.LastName} {order.BillingAddress.FirstName}");
                    queryParameters.Add("dsEmail", order.ShippingAddress.Email);
                    queryParameters.Add("dsMobPhone", order.ShippingAddress.PhoneNumber);
                    queryParameters.Add("dsCity", String.IsNullOrEmpty(order.ShippingAddress.City) ? string.Empty : order.ShippingAddress.City);
                    queryParameters.Add("dsStreet", order.ShippingAddress.Address1);
                    queryParameters.Add("dsHouse", order.ShippingAddress.AddressHouse);
                    queryParameters.Add("dsFlat", order.ShippingAddress.AddressFlat);
                }

                queryParameters.Add("ApiKey", "5e3c1e12a10249.58558322");
                queryParameters.Add("TestMode", "0");
                queryParameters.Add("order", BuildOrderString(order));
                queryParameters.Add("ExtOrderID", order.OrderNumber.ToString());
                queryParameters.Add("ExtOrderPaid", order.PaymentMethodSystemName  == "Payments.RoboKassa" ? "1" : "0");
                queryParameters.Add("ExtDeliveryCost", order.OrderShippingInclTax.ToString());
                queryParameters.Add("dsDelivery", GetDeliveryMethod(order.ShippingMethod));

                var url = QueryHelpers.AddQueryString("http://api.ds-platforma.ru/ds_order.php", queryParameters);

                using (var httpclient = new HttpClient())
                {
                    var response = await httpclient.GetAsync(url);
                    await _logger.InsertLog(LogLevel.Information, "Заказ отправлен в дропшиппинг", response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Ошибка при отправки заказа в дропшипинг", ex);
            }           
        }

        public string GetDeliveryMethod(string deliveryMethod)
        {
            switch (deliveryMethod)
            {
                case "СДЭК":
                    return "10";

                case "Почта России":
                    return "2";

                case "DPD":
                    return "8";

                case "Пикап в Самовывоз":
                    return "4";

                case "Доставка за пределами МКАД":
                    return "1";

                case "Доставка МСК курьером":
                    return "1";
            }
            return "";
        }

        public string BuildOrderString(Order order)
        {
            var r = new List<string>();
            foreach (var o in order.OrderItems)
            {
                var product = _productService.GetProductById(o.ProductId).Result;
                r.Add($"{product.Sku}-{o.Quantity}-{o.PriceInclTax.ToString(CultureInfo.CreateSpecificCulture("en-GB"))}");
            }
            return string.Join(".", r.ToArray());
        }      
    }
}
