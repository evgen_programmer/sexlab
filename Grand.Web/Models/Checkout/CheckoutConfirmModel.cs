﻿using Grand.Framework.Mvc.Models;
using System.Collections.Generic;

namespace Grand.Web.Models.Checkout
{
    public partial class CheckoutConfirmModel : BaseGrandModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();
            CheckoutBillingAddressModel = new CheckoutBillingAddressModel();
            CheckoutShippingAddressModel = new CheckoutShippingAddressModel();
            CheckoutShippingMethodModel = new CheckoutShippingMethodModel();
        }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public string MinOrderTotalWarning { get; set; }

        public CheckoutBillingAddressModel CheckoutBillingAddressModel { get; set; }

        public CheckoutShippingAddressModel CheckoutShippingAddressModel { get; set; }
        
        public CheckoutShippingMethodModel CheckoutShippingMethodModel { get; set; }

        public IList<string> Warnings { get; set; }
    }
}