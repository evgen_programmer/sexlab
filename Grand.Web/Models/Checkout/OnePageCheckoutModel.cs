﻿using Grand.Framework.Mvc.Models;
using Grand.Web.Models.ShoppingCart;

namespace Grand.Web.Models.Checkout
{
    public partial class OnePageCheckoutModel : BaseGrandModel
    {
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }
        public CheckoutBillingAddressModel BillingAddress { get; set; }
        public bool HasSinglePaymentMethod { get; set; }
        public ShoppingCartModel ShoppingCartModel { get; set; }
    }
}