﻿/*
** grandnode search
*/
var SearchAction = {
    EnterSearchTerms: '',
    MinLength: 0,
    UrlProductSearch: '',
    ShowProductImagesInSearch: true,

    init: function (enterSearchTerms) {
        this.EnterSearchTerms = enterSearchTerms;

        $("#small-search-box-form").submit(function (event) {
            if ($("#small-searchterms").val() === "") {
                alert(SearchAction.EnterSearchTerms);
                $("#small-searchterms").focus();
                event.preventDefault();
            }
        });
    },

    autocomplete: function (minLength, urlProductSearch, showProductImagesInSearch) {
        this.MinLength = minLength;
        this.UrlProductSearch = urlProductSearch;
        this.ShowProductImagesInSearch = showProductImagesInSearch;

        $('#small-searchterms').autocomplete({
            delay: 300,
            minLength: SearchAction.MinLength,
            source: function (request, response) {
                var category = '';
                if ($("#SearchCategoryId").length > 0) {
                    category = $("#SearchCategoryId").val();
                }
                $.ajax({
                    url: SearchAction.UrlProductSearch,
                    dataType: "json",
                    data: {
                        term: request.term,
                        categoryId: category
                    },
                    success: function (data) {
                        console.log(data);
                        response(data);
                        $('#category-result-search-box').html('');
                        $('#manufacturer-result-search-box').html('');
                        $('#product-result-search-box').html('');
                        let newManaf = "<h3>Производители</h3>";
                        let newCategory = "<h3>Категории</h3>";
                        let newProudct = "";
                        let countM = 0;
                        let countC = 0;
                        let countP = 0;
                        for (let i = 0; i < data.length; i ++) {
                            let item = data[i];
                            if (item.SearchType == "Manufacturer") {
                                newManaf += "<span>Комплект с <a href='"+item.Url+"'>"+item.Label+"</a></span>";
                                countM ++;
                            }
                            if (item.SearchType == "Category") {
                                newCategory += "<span>Комплект с <a href='"+item.Url+"'>"+item.Label+"</a></span>";
                                countC ++;
                            }
                            if (item.SearchType == "Product") {
                                newProudct += "<div class=\"d-flex one-product\" >\n"  +
                                    "                                 <img src='"+item.PictureUrl+"' alt=\"\">\n" +
                                    "                                 <div class=\"d-flex flex-column search-item-info justify-content-between\">\n" +
                                    "                                     <h4>"+item.Label+"</h4>\n" +
                                    "                                     <div class=\"d-flex kail justify-content-between\">\n" +
                                    "                                         <span>"+item.Price+"</span>\n" +
                                    "                                         <a href='"+item.Url+"' class=\"d-flex align-items-center\">\n" +
                                    "                                             <span>Смотреть</span>\n" +
                                    "                                             <svg width=\"20\" height=\"9\" viewBox=\"0 0 20 9\" fill=\"#f55b5b\" xmlns=\"http://www.w3.org/2000/svg\">\n" +
                                    "                                                 <path d=\"M18 4H0V5H18L15 8L16 9L20 4.5L15.5 0L14.5 0.5L18 4Z\"></path>\n" +
                                    "                                             </svg>\n" +
                                    "                                         </a>\n" +
                                    "                                     </div>\n" +
                                    "                                 </div>\n" +
                                    "                             </div>";
                                countP ++;
                            }
                        }
                        $('#category-result-search-box').html(newCategory);
                        $('#manufacturer-result-search-box').html(newManaf);
                        $('#product-result-search-box').html(newProudct);
                        $('#search-result-popup').removeClass('d-none');
                        
                        if (countM == 0) {
                            $('#manufacturer-result-search-box').html("<h3>Производители</h3><span>Нет результатов</span>");
                        }

                        if (countC == 0) {
                            $('#category-result-search-box').html("<h3>Категории</h3><span>Нет результатов</span>");
                        }

                        if (countP == 0) {
                            $('#product-result-search-box').html("<span>Нет результатов</span>");
                        }
                        
                    }
                });
            },
            appendTo: '.search-box',
            select: function (event, ui) {
                $("#small-searchterms").val(ui.item.Label);
                setLocation(ui.item.producturl);
                return false;
            }
        })
            .data("ui-autocomplete")._renderItem = function (ul, item) {
                //html encode
                var term = this.element.val();
                regex = new RegExp('(' + term + ')', 'gi');
                t = item.Label.replace(regex, "<b>$&</b>");
                desc = item.Desc.replace(regex, "<b>$&</b>");
                tin = $('#small-searchterms').val();
                var image = '';
                if (SearchAction.ShowProductImagesInSearch) {
                    image = "<img src='" + item.PictureUrl + "'>";
                }
                var pricereviewline = "";
                if (item.SearchType === "Product") {
                    pricereviewline = "<div class='d-flex justify-content-between w-100 ratings'><div class='price'>" + item.Price + "</div>";
                    if (item.AllowCustomerReviews)
                        pricereviewline += "<div class='rating-box'><div class='rating' style='width: " + item.Rating + "%'></div></div>";
                    pricereviewline += "</div>";
                }
                return $("<li data-type='" + item.SearchType + "' class='list-group-item' ></li>")
                    .data("item.autocomplete", item)
                    .append("<a class='d-inline-flex align-items-start w-100' href='" + item.Url + "' class='generalImg'>" + image + "<div class='container-off col px-0'><div class='product-in'></div><div class='in-separator'>in</div><div class='product-title'>" + t + "</div>" + pricereviewline + "</div></a>")
                    .appendTo(ul)
                    .find(".product-in").text(tin);
            };

    }
}
function closeSearchBox() {
    if (window.matchMedia('(min-width: 992px)').matches) {
        $(window).click(function () {
            $('.advanced-search-results').removeClass("open");
        });
        $('.advanced-search-results').click(function (event) {
            event.stopPropagation();
        });
    }
}
$(document).ready(function () {

    closeSearchBox();

    $(window).resize(function () {
        closeSearchBox();
    });

});