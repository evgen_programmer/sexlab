$(function() {
    "use strict";

    var nav_offset_top = $('header').height() + 50;
    var start_offset_header = $('header').height();
    var scrollPrev = 0;
    /*-------------------------------------------------------------------------------
	  Navbar 
	-------------------------------------------------------------------------------*/

    //* Navbar Fixed 
    

    function navbarFixed(){

        if ($(window).width() >= 992) {
            if ( $('.header_area').length ){
                $(window).scroll(function() {
                    let scroll = $(window).scrollTop();
                    if (scroll >= nav_offset_top) {
                        $('body').css('padding-top', start_offset_header);
                        $(".header_area").addClass("navbar_fixed");
                    } else {
                        $('body').css('padding-top', 0);
                        $(".header_area").removeClass("navbar_fixed");
                    }
                });
            }
        }
        else {
            if ($('.header_area').length) {
                $(window).scroll(function () {
                    let scroll = $(window).scrollTop();

                    if (scroll > nav_offset_top && scroll > scrollPrev) {
                        $(".header_area").addClass("out");
                    } else {
                        $(".header_area").removeClass("out");
                    }
                    scrollPrev = scroll;
                });
            }
        }
    }
    navbarFixed();

    $('#minus').click(function() {
        if ($('#productqty').val() != 1) {
            $('#productqty').val(parseInt($('#productqty').val()) - 1);
            $(this).next().text($('#productqty').val());
        }
    });

    $('#plus').click( function() {
        $('#productqty').val(parseInt($('#productqty').val()) + 1);
        $(this).prev().text($('#productqty').val());
    });

    $(document).ready(function(){
        $('.center').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 3,
            autoplay: false,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        /*-------------------------------------------------------------------------------
            NEWS slider
        -------------------------------------------------------------------------------*/
        $('.content-slide').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 4,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });


    if ($('.blog-slider').length) {
        $('.blog-slider').owlCarousel({
            loop: true,
            margin: 30,
            items: 1,
            nav: true,
            autoplay: 2500,
            smartSpeed: 1500,
            dots: false,
            responsiveClass: true,
            navText : ["<div class='blog-slider__leftArrow'><img src='img/home/left-arrow.png'></div>","<div class='blog-slider__rightArrow'><img src='img/home/right-arrow.png'></div>"],
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        })
    }

    /*-------------------------------------------------------------------------------
        testimonial slider
      -------------------------------------------------------------------------------*/
    if ($('.testimonial').length) {
        $('.testimonial').owlCarousel({
            loop: true,
            margin: 30,
            items: 5,
            nav: false,
            dots: true,
            responsiveClass: true,
            slideSpeed: 300,
            paginationSpeed: 500,
            responsive: {
                0: {
                    items: 1
                }
            }
        })
    }

});



$('.header_area .menu-tail > li:first-child').click(function (e) {
    var $cat = $(".header_area .catalog-nav");

    if ($cat.css('display') != 'block') {
        $cat.show(300);

        var firstClick = true;
        $(document).bind('click.myEvent', function (e) {
            if (!firstClick && $(e.target).closest('.header_area .catalog-nav').length == 0) {
                $cat.hide(300);
                $(document).unbind('click.myEvent');
            }
            firstClick = false;
        });
    }

    e.preventDefault();
});


$(document).ready(function(){
    $('.slide-content').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 2,
        adaptiveHeight: true,
        prevArrow:'<button class="PrevArrow">' +
            '<svg width="20" height="9" viewBox="0 0 20 9" fill="#C4C4C4" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M4 0L0 4.5L4 9L5 8L2 5H20V4H2L5 1L4 0Z"></path>\n' +
            '</svg></button>',
        nextArrow:'<button class="NextArrow"><svg width="20" height="9" viewBox="0 0 20 9" fill="#C4C4C4" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M16 0L20 4.5L16 9L15 8L18 5H0V4H18L15 1L16 0Z"></path>\n' +
            '</svg></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }

            }
        ]
    });

    $('.slide-hit').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 2,
        adaptiveHeight: true,
        prevArrow:'<button class="PrevArrow">' +
            '<svg width="20" height="9" viewBox="0 0 20 9" fill="#C4C4C4" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M4 0L0 4.5L4 9L5 8L2 5H20V4H2L5 1L4 0Z"></path>\n' +
            '</svg></button>',
        nextArrow:'<button class="NextArrow"><svg width="20" height="9" viewBox="0 0 20 9" fill="#C4C4C4" xmlns="http://www.w3.org/2000/svg">\n' +
            '<path d="M16 0L20 4.5L16 9L15 8L18 5H0V4H18L15 1L16 0Z"></path>\n' +
            '</svg></button>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    centerMode: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });

});
